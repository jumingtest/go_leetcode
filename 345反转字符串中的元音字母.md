## 题目：
给你一个字符串 s ，仅反转字符串中的所有元音字母，并返回结果字符串。

元音字母包括 'a'、'e'、'i'、'o'、'u'，且可能以大小写两种形式出现。

示例 1：

输入：s = "hello"
输出："holle"
示例 2：

输入：s = "leetcode"
输出："leotcede"
 
提示：

1 <= s.length <= 3 * 105
s 由 可打印的 ASCII 字符组成

***
## 代码：
```go
func reverseVowels(s string) string {  
	ss := []byte(s)  
	i ,j := 0 , len(ss) -1  
	for i < j {  
	    b1,b2 := false,false  
		for m := i ; m <=len(ss) -1 ; m ++ {  
	        if IsO(ss[m]) {  
	            i = m  
		        b1 = true  
				break 
			}  
	    }  
	    for n := j ; n >= 0 ; n-- {  
	        if IsO(ss[n]) {  
	            j = n  
	            b2 = true  
				break 
			}  
	    }  
	    if i <= j && b1 && b2{  
	        ss[i],ss[j] = ss[j],ss[i]  
	    }  
	    i++  
	    j--  
	}  
	return string(ss)  
}  
  
  
func IsO(s byte) bool {  
	c := []byte{'a','e','i','o','u','A','E','I','O','U'}  
	for i := 0 ; i <len(c) ; i ++ {  
	    if c[i] == s {  
	        return true  
		}  
	}  
	return false  
}
```

```go
func isMatched(b byte) bool {  
	switch (b) {  
	case 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U':  
	    return true  
	default:  
	    return false  
	}  
}  
  
func reverseVowels(s string) string {  
	l, r, bs := 0, len(s) - 1, []byte(s)  
  
	for l < r {  
	    for l < len(s) {  
	        if isMatched(bs[l]) == true {  
		        break  
			}  
	        l++  
	    }  
  
	    for r >= 0 {  
	        if isMatched(bs[r]) == true {  
		        break  
			}  
	        r--  
	    }  
  
	    if l < r {  
	        bs[l], bs[r] = bs[r], bs[l]  
	        l++  
	        r--  
	    }  
	}  
	return string(bs)  
}
```