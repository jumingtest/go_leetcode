## 题目：
实现一个MyQueue类，该类用两个栈来实现一个队列。

示例：

MyQueue queue = new MyQueue();

queue.push(1);
queue.push(2);
queue.peek();  // 返回 1
queue.pop();   // 返回 1
queue.empty(); // 返回 false

说明：

你只能使用标准的栈操作 -- 也就是只有 push to top, peek/pop from top, size 和 is empty 操作是合法的。
你所使用的语言也许不支持栈。你可以使用 list 或者 deque（双端队列）来模拟一个栈，只要是标准的栈操作即可。
假设所有操作都是有效的 （例如，一个空的队列不会调用 pop 或者 peek 操作）。

***
## 代码：
```go
type MyQueue struct {  
	heap []int  
}  
  
  
func Constructor() MyQueue {  
	return MyQueue{  
	    heap:make([]int,0),  
	}  
}  
  
  
func (this *MyQueue) Push(x int)  {  
	this.heap = append(this.heap,x)  
}  
  
  
func (this *MyQueue) Pop() int {  
	tmp := this.heap[0]  
	this.heap = this.heap[1:]  
	return tmp  
}  
  
  
func (this *MyQueue) Peek() int {  
	return this.heap[0]  
}  
  
  
func (this *MyQueue) Empty() bool {  
	return len(this.heap) == 0  
}
```