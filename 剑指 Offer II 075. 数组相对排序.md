## 题目：
给定两个数组，arr1 和 arr2，

arr2 中的元素各不相同
arr2 中的每个元素都出现在 arr1 中
对 arr1 中的元素进行排序，使 arr1 中项的相对顺序和 arr2 中的相对顺序相同。未在 arr2 中出现过的元素需要按照升序放在 arr1 的末尾。

示例：

输入：arr1 = [2,3,1,3,2,4,6,7,9,2,19], arr2 = [2,1,4,3,9,6]
输出：[2,2,2,1,4,3,3,9,6,7,19]
 
提示：

1 <= arr1.length, arr2.length <= 1000
0 <= arr1[i], arr2[i] <= 1000
arr2 中的元素 arr2[i] 各不相同
arr2 中的每个元素 arr2[i] 都出现在 arr1 中

***
## 代码：
```go
func relativeSortArray(arr1 []int, arr2 []int) []int {  
	maps := make(map[int]int,0)  
	for i,v := range arr2 {  
	    maps[v] = i  
	}  
	tmp1 , tmp2 := []int{} , []int{}  
	for _ , v := range arr1 {  
	    if _,ok := maps[v];ok {  
	        tmp1 = append(tmp1,v)  
	    }else{  
	        tmp2 = append(tmp2,v)  
	    }  
	}  
	sort.Slice(tmp1,func(i,j int) bool {  
	    return maps[tmp1[i]] < maps[tmp1[j]]  
	})  
	sort.Ints(tmp2)  
	return append(tmp1,tmp2...)  
}
```