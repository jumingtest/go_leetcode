## 题目：
「力扣挑战赛」心算项目的挑战比赛中，要求选手从 N 张卡牌中选出 cnt 张卡牌，若这 cnt 张卡牌数字总和为偶数，则选手成绩「有效」且得分为 cnt 张卡牌数字总和。
给定数组 cards 和 cnt，其中 cards[i] 表示第 i 张卡牌上的数字。 请帮参赛选手计算最大的有效得分。若不存在获取有效得分的卡牌方案，则返回 0。

示例 1：

输入：cards = [1,2,8,9], cnt = 3

输出：18

解释：选择数字为 1、8、9 的这三张卡牌，此时可获得最大的有效得分 1+8+9=18。

示例 2：

输入：cards = [3,3,1], cnt = 1

输出：0

解释：不存在获取有效得分的卡牌方案。

提示：

1 <= cnt <= cards.length <= 10^5
1 <= cards[i] <= 1000

***
## 代码：
```go
func max(a, b int) int {  
	if a > b {  
	    return a  
	}  
	return b  
}  
  
func maxmiumScore(cards []int, cnt int) int {  
	sort.Ints(cards)  
  
	odds := []int{0}  
	evens := []int{0}  
  
	for _, val := range cards {  
	    if val%2 == 0 {  
	        evens = append(evens, evens[len(evens)-1]+val)  
	    } else {  
	        odds = append(odds, odds[len(odds)-1]+val)  
	    }  
	}  
  
	res := 0  
	for k := 0; k < len(odds); k += 2 {  
	    evenNum := cnt-k  
	    if 0 <= evenNum && evenNum < len(evens) {  
	        res = max(res, odds[k]+evens[evenNum])  
	    }  
	}  
  
	return res  
}
```