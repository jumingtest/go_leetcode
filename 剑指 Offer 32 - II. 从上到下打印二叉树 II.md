## 题目：
从上到下按层打印二叉树，同一层的节点按从左到右的顺序打印，每一层打印到一行。

例如:
给定二叉树: [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
返回其层次遍历结果：

[
  [3],
  [9,20],
  [15,7]
]
提示：

节点总数 <= 1000

****
## 代码：
```go
func levelOrder(root *TreeNode) [][]int {  
	maps := map[int][]int{}  
	appenVal(root,0,maps)  
	res := make([][]int,len(maps))  
	for index,val := range maps {  
	    res[index] = val  
	}  
	return res  
}  
  
func appenVal(root *TreeNode , level int , maps map[int][]int){  
	if root == nil {  
	    return  
	}  
	maps[level] = append(maps[level],root.Val)  
	appenVal(root.Left,level+1,maps)  
	appenVal(root.Right,level+1,maps)  
}
```