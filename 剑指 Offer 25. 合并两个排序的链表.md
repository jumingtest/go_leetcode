## 题目：
输入两个递增排序的链表，合并这两个链表并使新链表中的节点仍然是递增排序的。

示例1：

输入：1->2->4, 1->3->4
输出：1->1->2->3->4->4
限制：

0 <= 链表长度 <= 1000

***
## 代码：
```go
func mergeTwoLists(list1 *ListNode, list2 *ListNode) *ListNode {  
	if list1 == nil{  
	    return list2  
	}  
	if list2 == nil{  
	    return list1  
	}  
	var res *ListNode  
	if list1.Val >= list2.Val{  
	    res = list2  
	    res.Next = mergeTwoLists(list1,list2.Next)  
	}else{  
	    res = list1  
	    res.Next = mergeTwoLists(list1.Next,list2)  
	}  
	return res  
}
```