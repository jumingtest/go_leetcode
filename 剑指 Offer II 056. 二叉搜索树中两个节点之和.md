## 题目：
给定一个二叉搜索树的 根节点 root 和一个整数 k , 请判断该二叉搜索树中是否存在两个节点它们的值之和等于 k 。假设二叉搜索树中节点的值均唯一。

示例 1：

输入: root = [8,6,10,5,7,9,11], k = 12
输出: true
解释: 节点 5 和节点 7 之和等于 12
示例 2：

输入: root = [8,6,10,5,7,9,11], k = 22
输出: false
解释: 不存在两个节点值之和为 22 的节点
 
提示：

二叉树的节点个数的范围是  [1, 104].
-104 <= Node.val <= 104
root 为二叉搜索树
-105 <= k <= 105

****
## 代码：
```go
func findTarget(root *TreeNode, k int) bool {  
	m   := make(map[int]bool)  
	ans := false  
	var dfs func(*TreeNode)  
	dfs = func(node *TreeNode) {  
	    if node != nil {  
	        m[node.Val] = true  
			if m[k-node.Val] && k != 2*node.Val{  
		        ans = true  
				return 
			}  
	        dfs(node.Left)  
	        dfs(node.Right)  
	    }  
	}  
	dfs(root)  
	return ans  
}
```