## 题目：
给定一个非空的正整数数组 nums ，请判断能否将这些数字分成元素和相等的两部分。

示例 1：

输入：nums = [1,5,11,5]
输出：true
解释：nums 可以分割成 [1, 5, 5] 和 [11] 。
示例 2：

输入：nums = [1,2,3,5]
输出：false
解释：nums 不可以分为和相等的两部分

提示：

1 <= nums.length <= 200
1 <= nums[i] <= 100
 
***
## 代码：
```go
func canPartition(nums []int) bool {  
	sum := 0  
	for _, v := range nums {  
	    sum += v  
	}  
	if sum % 2 == 1 {   
	    return false  
	}  
	half := sum / 2  
	dp := make([][]bool, len(nums)+1)  
	for i := 0; i <= len(nums); i++ {  
	    dp[i] = make([]bool, half+1)  
	    dp[i][0] = true  
	}  
	for i := 1; i <= len(nums); i++ {  
	    for j := 1; j <= half; j++ {  
	        if j >= nums[i-1] {  
	            dp[i][j] = dp[i-1][j] || dp[i-1][j-nums[i-1]]  
	        } else {  
	            dp[i][j] = dp[i-1][j]  
	        }  
	    }  
	}  
	return dp[len(nums)][half]  
}
```