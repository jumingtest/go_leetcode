## 题目:
给定一个二叉树，找出其最小深度。

最小深度是从根节点到最近叶子节点的最短路径上的节点数量。

说明：叶子节点是指没有子节点的节点。

示例 1：

输入：root = [3,9,20,null,null,15,7]
输出：2
示例 2：

输入：root = [2,null,3,null,4,null,5,null,6]
输出：5
 
提示：

树中节点数的范围在 [0, 105] 内
-1000 <= Node.val <= 1000

***
## 代码：
```go
func minDepth(root *TreeNode) int {  
	if root == nil {  
	    return 0  
	}  
	left := minDepth(root.Left)  
	right := minDepth(root.Right)  
	if left == 0 || right == 0 {  
	    return 1 + left + right  
	}  
	return min(left,right) + 1  
}  
  
func min(a,b int) int {  
	if a <= b {  
	    return a  
	}  
	return b  
}
```