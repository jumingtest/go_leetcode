## 题目：
给定一个链表的 头节点 head ，请判断其是否为回文链表。

如果一个链表是回文，那么链表节点序列从前往后看和从后往前看是相同的。

示例 1：

输入: head = [1,2,3,3,2,1]
输出: true
示例 2：

输入: head = [1,2]
输出: false
 
提示：

链表 L 的长度范围为 [1, 105]
0 <= node.val <= 9
 
***
## 代码：
```go
func isPalindrome(head *ListNode) bool {  
	if head == nil {  
	    return true  
	}  
	tmp := []int{}  
	for head != nil {  
	    tmp = append(tmp,head.Val)  
	    head = head.Next  
	}  
	for i :=0 ; i <len(tmp) ; i++ {  
	    if tmp[i] != tmp[len(tmp)-1-i]{  
	        return false  
		}  
	}  
	return true  
}
```