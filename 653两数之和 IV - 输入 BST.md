## 题目：
给定一个二叉搜索树 root 和一个目标结果 k，如果 BST 中存在两个元素且它们的和等于给定的目标结果，则返回 true。

示例 1：

输入: root = [5,3,6,2,4,null,7], k = 9
输出: true
示例 2：

输入: root = [5,3,6,2,4,null,7], k = 28
输出: false
 
提示:

二叉树的节点个数的范围是  [1, 104].
-104 <= Node.val <= 104
root 为二叉搜索树
-105 <= k <= 105

***
## 代码：
```go
func findTarget(root *TreeNode, k int) bool {  
	tmp := getAll(root)  
	for i := 0 ; i < len(tmp) ;i++ {  
	    for j  := i+1 ; j <len(tmp) ; j++ {  
	        if tmp[j] == k -tmp[i] {  
		        return true  
			}  
	    }  
	}  
	return false  
}  
  
func getAll(root *TreeNode) []int{  
	if root == nil {  
	    return []int{}  
	}  
	tmp := []int{root.Val}  
	tmp = append(tmp,getAll(root.Left)...)  
	tmp = append(tmp,getAll(root.Right)...)  
	return tmp  
}
```

```go
func findTarget(root *TreeNode, k int) bool {  
	set := map[int]struct{}{}  
	var find func(*TreeNode) bool  
	find = func(n *TreeNode) bool {  
	    if n == nil {  
	        return false  
		}  
	    if _, ok := set[k-n.Val]; ok {  
	        return true  
		}  
	    set[n.Val] = struct{}{}  
	    return find(n.Left) || find(n.Right)  
	}  
	return find(root)  
}
```