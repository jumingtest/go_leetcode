## 题目：
给定一个非空字符串 s，请判断如果 最多 从字符串中删除一个字符能否得到一个回文字符串。

示例 1:

输入: s = "aba"
输出: true
示例 2:

输入: s = "abca"
输出: true
解释: 可以删除 "c" 字符 或者 "b" 字符
示例 3:

输入: s = "abc"
输出: false

提示:

1 <= s.length <= 105
s 由小写英文字母组成

***
## 代码：
```go
func test(s string, l, r int, cnt int) bool {  
	for l < r {  
	    if s[l] != s[r] {  
	        if cnt == 0 {  
		        return false  
			}  
	        return test(s, l+1, r, cnt - 1) || test(s, l, r - 1, cnt -1)  
	    } else {  
	        l++  
	        r--  
	    }  
	}  
	return true  
}  
  
func validPalindrome(s string) bool {  
	return test(s, 0, len(s) - 1, 1)  
}
```