## 题目：
给定两个 01 字符串 a 和 b ，请计算它们的和，并以二进制字符串的形式输出。

输入为 非空 字符串且只包含数字 1 和 0。

示例 1:

输入: a = "11", b = "10"
输出: "101"
示例 2:

输入: a = "1010", b = "1011"
输出: "10101"

提示：

每个字符串仅由字符 '0' 或 '1' 组成。
1 <= a.length, b.length <= 10^4
字符串如果不是 "0" ，就都不含前导零。
 
***
## 代码：
```go
func addBinary(a string, b string) string {  
	if len(a) >= len(b){  
	    b = strings.Repeat("0",len(a)-len(b)) + b  
	}else{  
	    a = strings.Repeat("0",len(b)-len(a)) + a  
	}  
	bytea := []byte(a)  
	byteb := []byte(b)  
	for i := len(bytea)-1 ; i >=0 ; i-- {  
	    if int(bytea[i]) + int(byteb[i]) >= 98 && i > 0{  
	        bytea[i] = byte(int(bytea[i]) + int(byteb[i])-50)  
	        bytea[i-1] += 1  
		}else{  
	        bytea[i] = byte(int(bytea[i]) + int(byteb[i])-48)  
	    }  
	}  
	if bytea[0] >= 50{  
	    bytea[0] = bytea[0]- 2  
		bytea = append([]byte("1"),bytea...)  
	}  
	return string(bytea)  
}
```