## 题目：
数组中有一个数字出现的次数超过数组长度的一半，请找出这个数字。

你可以假设数组是非空的，并且给定的数组总是存在多数元素。

示例 1:

输入: [1, 2, 3, 2, 2, 2, 5, 4, 2]
输出: 2

限制：

1 <= 数组长度 <= 50000

***
## 代码：
```go
func majorityElement(nums []int) int {  
	val := nums[0]  
	count := 0  
	for _ , num := range nums {  
	    if num == val {  
	        count ++  
	    } else{  
	        count --  
	        if count <= 0 {  
	            val   = num  
	            count = 1  
			}  
	    }  
	}  
	return val  
}
```