## 题目：
统计一个数字在排序数组中出现的次数。

示例 1:

输入: nums = [5,7,7,8,8,10], target = 8
输出: 2
示例 2:

输入: nums = [5,7,7,8,8,10], target = 6
输出: 0
 
提示：

0 <= nums.length <= 105
-109 <= nums[i] <= 109
nums 是一个非递减数组
-109 <= target <= 109

***
## 代码：
```go
func search(nums []int, target int) int {  
	count := 0  
	for _ , num := range nums {  
	    if num == target {  
	        count ++  
	    }  
	}  
	return count  
}
```

```go
func search(nums []int, target int) int {  
	i := 0  
	j := len(nums) - 1  
	for i <= j {  
	    m := (i+j)/2  
		if nums[m] > target {  
	        j = m - 1  
		} else if nums[m] < target {  
	        i = m + 1  
		} else {  
	        i = m - 1  
			for i >= 0 && nums[i] == target {  
		        i--  
		    }  
		    j = m + 1  
			for j < len(nums) && nums[j] == target {  
		        j++  
	        }  
	        return j - i - 1  
		}  
	}  
	return 0  
}
```