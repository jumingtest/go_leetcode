## 题目：
示例 1：

输入：s = "K1:L2"
输出：["K1","K2","L1","L2"]
解释：
上图显示了列表中应该出现的单元格。
红色箭头指示单元格的出现顺序。
示例 2：

输入：s = "A1:F1"
输出：["A1","B1","C1","D1","E1","F1"]
解释：
上图显示了列表中应该出现的单元格。 
红色箭头指示单元格的出现顺序。
 
提示：

s.length == 5
'A' <= s[0] <= s[3] <= 'Z'
'1' <= s[1] <= s[4] <= '9'
s 由大写英文字母、数字、和 ':' 组成

****
## 代码：
```go
func cellsInRange(s string) []string {  
	left,right := s[:2],s[3:]  
	res := []string{}  
	for a := left[0] ; a <= right[0] ; a ++{  
		for b := left[1] ; b <= right[1] ; b++ {  
	        tmp := string(a) + string(b)  
	        res = append(res,tmp)  
	    }  
	}  
	return res  
}
```


