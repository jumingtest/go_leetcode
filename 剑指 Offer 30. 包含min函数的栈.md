## 题目：
定义栈的数据结构，请在该类型中实现一个能够得到栈的最小元素的 min 函数在该栈中，调用 min、push 及 pop 的时间复杂度都是 O(1)。

示例:

MinStack minStack = new MinStack();
minStack.push(-2);
minStack.push(0);
minStack.push(-3);
minStack.min();   --> 返回 -3.
minStack.pop();
minStack.top();      --> 返回 0.
minStack.min();   --> 返回 -2.
 
提示：

各函数的调用总次数不超过 20000 次

****
## 代码：
```go
type MinStack struct {  
	Num    []int  
	MinNum []int  
}  
  
  
func Constructor() MinStack {  
	return MinStack{Num:[]int{},MinNum:[]int{}}  
}  
  
func (this *MinStack) Push(x int)  {  
	this.Num = append(this.Num,x)  
	if len(this.MinNum) == 0 {  
	    this.MinNum = append(this.MinNum,x)  
	}else{  
	    if this.MinNum[len(this.MinNum)-1] > x {  
	        this.MinNum = append(this.MinNum,x)  
	    }else{  
	        this.MinNum = append(this.MinNum,this.MinNum[len(this.MinNum)-1])  
	    }  
	}  
}  
  
  
func (this *MinStack) Pop()  {  
	if len(this.Num) == 0 {  
	    return  
	}  
	this.Num = this.Num[:len(this.Num)-1]  
	this.MinNum = this.MinNum[:len(this.MinNum)-1]  
}  
  
  
func (this *MinStack) Top() int {  
	return this.Num[len(this.Num)-1]  
}  
  
  
func (this *MinStack) Min() int {  
	return this.MinNum[len(this.MinNum)-1]  
}
```