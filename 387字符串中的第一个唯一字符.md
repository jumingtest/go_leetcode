## 题目：
给定一个字符串 s ，找到 它的第一个不重复的字符，并返回它的索引 。如果不存在，则返回 -1 。

示例 1：

输入: s = "leetcode"
输出: 0
示例 2:

输入: s = "loveleetcode"
输出: 2
示例 3:

输入: s = "aabb"
输出: -1
 

提示:

1 <= s.length <= 105
s 只包含小写字母

***
## 代码：
```go
func firstUniqChar(s string) int {  
	res := -1  
	ss := []byte(s)  
	for i := 0 ; i < len(ss) ; i++ {  
	    tmp := true  
		for j := 0 ; j < len(ss) ; j++ {  
	        if i == j {  
		        continue  
			}  
	        if ss[i] == ss[j] || j == len(ss){  
	            tmp = false  
				break 
			}  
	    }  
	    if tmp {  
	        res = i  
	        break  
		}  
	}  
	return res  
}
```