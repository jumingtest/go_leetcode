## 题目：
给定一棵二叉搜索树，请找出其中第 k 大的节点的值。

示例 1:

输入: root = [3,1,4,null,2], k = 1
   3
  / \
 1   4
  \
   2
输出: 4
示例 2:

输入: root = [5,3,6,2,4,null,null,1], k = 3
       5
      / \
     3   6
    / \
   2   4
  /
 1
输出: 4
 
限制：

1 ≤ k ≤ 二叉搜索树元素个数

***
## 代码：
```go
func kthLargest(root *TreeNode, k int) int {  
	nums := getVal(root)  
	sort.Ints(nums)  
	return nums[len(nums)-k]  
}  

func getVal(root *TreeNode)  []int{  
	if root == nil {  
	    return []int{}  
	}  
	res := []int{root.Val}  
	res  = append(res,getVal(root.Left)...)  
	res  = append(res,getVal(root.Right)...)  
	return res  
}
```