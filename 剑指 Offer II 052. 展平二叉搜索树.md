## 题目：
给你一棵二叉搜索树，请 按中序遍历 将其重新排列为一棵递增顺序搜索树，使树中最左边的节点成为树的根节点，并且每个节点没有左子节点，只有一个右子节点。

示例 1：

输入：root = [5,3,6,2,4,null,8,1,null,null,null,7,9]
输出：[1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]
示例 2：

输入：root = [5,1,7]
输出：[1,null,5,null,7]
 
提示：

树中节点数的取值范围是 [1, 100]
0 <= Node.val <= 1000
 
***
## 代码：
```go
func GetVal(root *TreeNode) []int{  
	if root == nil {  
	    return []int{}  
	}  
	left   := GetVal(root.Left)  
	right  := GetVal(root.Right)  
	center := append(left,root.Val)  
	return append(center,right...)  
}  
  
  
func increasingBST(root *TreeNode) *TreeNode {  
	vals := GetVal(root)  
		if len(vals) == 0 {  
	    return nil  
	}  
	res := &TreeNode{Val:vals[len(vals)-1]}  
	for i := len(vals)-2 ; i >= 0 ;i-- {  
	    res = &TreeNode{Val:vals[i],Right:res}  
	}  
	return res  
}
```