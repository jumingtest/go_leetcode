## 题目：
输入一个矩阵，按照从外向里以顺时针的顺序依次打印出每一个数字。

示例 1：

输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
输出：[1,2,3,6,9,8,7,4,5]
示例 2：

输入：matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
输出：[1,2,3,4,8,12,11,10,9,5,6,7]
 
限制：

0 <= matrix.length <= 100
0 <= matrix[i].length <= 100

***
## 代码：
```go
func spiralOrder(matrix [][]int) []int {  
	if len(matrix)==0{  
	    return nil  
	}  
	ans := make([]int,0)  
	top,bottom,left,right:=0,len(matrix)-1,0,len(matrix[0])-1  
	for top<=bottom&&left<=right{  
	    for i := left; i <= right; i++ {  
		    ans = append(ans, matrix[top][i])  
	    }  
	    for i := top+1; i <= bottom; i++ {  
	        ans = append(ans, matrix[i][right])  
	    }  
	    if left < right && top < bottom {  
	        for i := right-1; i > left; i-- {  
		        ans = append(ans, matrix[bottom][i])  
	        }  
	        for i := bottom; i > top; i-- {  
	            ans = append(ans, matrix[i][left])  
	        }  
	    }  
	    left++  
	    top++  
	    right--  
	    bottom--  
	}  
	return ans  
}
```