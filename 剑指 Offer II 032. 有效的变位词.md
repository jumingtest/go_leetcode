## 题目：
给定两个字符串 s 和 t ，编写一个函数来判断它们是不是一组变位词（字母异位词）。

注意：若 s 和 t 中每个字符出现的次数都相同且字符顺序不完全相同，则称 s 和 t 互为变位词（字母异位词）。

示例 1:

输入: s = "anagram", t = "nagaram"
输出: true
示例 2:

输入: s = "rat", t = "car"
输出: false
示例 3:

输入: s = "a", t = "a"
输出: false
 
提示:

1 <= s.length, t.length <= 5 * 104
s and t 仅包含小写字母
 
***
## 代码：
```go
func isAnagram(s string, t string) bool {  
	if len(s) != len(t) || s ==t{  
	    return false  
	}  
	maps := make(map[byte]int,0)  
	for i := 0 ; i <len(s) ; i++ {  
	    maps[byte(s[i])] ++  
	}  
	for i := 0 ; i <len(t) ; i++ {  
	    val,ok := maps[byte(t[i])]  
	    if !ok || val < 1 {  
	        return false  
		}  
	    maps[byte(t[i])] --  
	}  
	return true  
}
```