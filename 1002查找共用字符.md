## 题目：
给你一个字符串数组 words ，请你找出所有在 words 的每个字符串中都出现的共用字符（ 包括重复字符），并以数组形式返回。你可以按 任意顺序 返回答案。
 
示例 1：

输入：words = ["bella","label","roller"]
输出：["e","l","l"]
示例 2：

输入：words = ["cool","lock","cook"]
输出：["c","o"]
 
提示：

1 <= words.length <= 100
1 <= words[i].length <= 100
words[i] 由小写英文字母组成

***
## 代码：
```go
func commonChars(words []string) []string {  
	sort.Slice(words,func(i,j int) bool{  
	    return len(words[i]) <len(words[j])  
	})  
	tmp := make([][]byte,0)  
	for i := 1 ; i <len(words) ; i++ {  
	    tmp = append(tmp,[]byte(words[i]))  
	}  
	
	res := []string{}  
	for _,v := range words[0]{  
	    has := true  
		for index,val := range tmp {  
	        f := getFirst(val,byte(v))  
	        if f == -1 {  
		        has = false  
			}else{  
		        tmp[index][f] = 0  
			}  
	    }  
	    if has {  
	        res = append(res,string(v))  
	    }  
	}  
	return res  
}  
  
func getFirst(s []byte,b byte) int {  
	for index,val := range s {  
	    if val == b {  
	        return index  
	    }  
	}  
	return -1  
}
```