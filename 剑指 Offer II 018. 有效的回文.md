## 题目：
给定一个字符串 s ，验证 s 是否是 回文串 ，只考虑字母和数字字符，可以忽略字母的大小写。

本题中，将空字符串定义为有效的 回文串 。

示例 1:

输入: s = "A man, a plan, a canal: Panama"
输出: true
解释："amanaplanacanalpanama" 是回文串
示例 2:

输入: s = "race a car"
输出: false
解释："raceacar" 不是回文串
 
提示：

1 <= s.length <= 2 * 105
字符串 s 由 ASCII 字符组成

***
## 代码：
```go
func isPalindrome(s string) bool {  
	builder := strings.Builder{}  
	for _,val := range s {  
	    if byte(val) >= 48 && byte(val) <= 57{  
	        builder.WriteByte(byte(val))  
	    }  
	    if byte(val) >= 65 && byte(val) <= 90{  
	        builder.WriteByte(byte(val))  
	    }  
	    if byte(val) >= 97 && byte(val) <= 122{  
	        builder.WriteByte(byte(val))  
	    }  
	}  
	s = strings.ToLower(builder.String())  
	for i := 0 ; i < len(s) - 1 ; i++ {  
	    if s[i] != s[len(s)-i-1]{  
	        return false  
		}  
	}  
	return  true  
}
```