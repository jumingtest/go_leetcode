## 题目：
在字符串 s 中找出第一个只出现一次的字符。如果没有，返回一个单空格。 s 只包含小写字母。

示例 1:

输入：s = "abaccdeff"
输出：'b'
示例 2:

输入：s = "" 
输出：' '
 
限制：

0 <= s 的长度 <= 50000

***
## 代码：
```go
func firstUniqChar(s string) byte {  
	maps := map[byte]int{}  
	for _ , ss := range s {  
	    maps[byte(ss)] ++  
	}  
	for _ , ss := range s {  
	    num , ok := maps[byte(ss)] ; if ok && num == 1 {  
	        return byte(ss)  
	    }  
	}  
	return ' '  
}
```

```go
func firstUniqChar(s string) byte {  
	res := [26]int{}  
	for _,v := range s{  
	    res[v-'a']++  
	}  
	for i,v := range s{  
	    if res[v-'a'] ==1{  
	        return s[i]  
	    }  
	}  
	return ' '  
}
```